import aiida.common
import numpy as np
from aiida.common import aiidalogger
from aiida.orm.workflow import Workflow
from aiida.orm import Code, Computer
from aiida.orm import DataFactory
from ase.io import read, write
from aiida.orm import load_node

#number of pools
def getpools(nodes,cpu_node,kpoints):
    npools=1
    return npools

class ScananoribbonWorkflow(Workflow):
    def __init__(self, **kwargs):
        super(ScananoribbonWorkflow, self).__init__(**kwargs)



    def get_structure(self):
        ParameterData = DataFactory('parameter')
        params = self.get_parameters()

        StructureData = DataFactory('structure')
        r=read(params['inpcoord'])  
        for i in r:
            if i.position[1] > 20.4 and i.position[1] < 21.0 :
                i.tag=1
            elif i.position[1] > 9.0  and  i.position[1] < 9.8 :
                i.tag=2
        s = StructureData(ase=r)
        s.store()
        return s
    def get_pw_vc_parameters(self):
        ParameterData = DataFactory('parameter')
        parameters = ParameterData(dict={
                  'CONTROL': {
                      'calculation': 'vc-relax',
                      'restart_mode': 'from_scratch',
                      'wf_collect': True,
                      'forc_conv_thr':0.0001,
                      'nstep':500,
                      },
                  'SYSTEM': {
                      'ecutwfc': 50.,
                      'ecutrho': 400.,
                      'occupations':'smearing',
                      'degauss': 0.001,
                      },
                  'ELECTRONS': {
                      'conv_thr': 1.e-8,
                      'mixing_beta':0.25,
                      'electron_maxstep':50,
                      'scf_must_converge':False,
                      },
                  'CELL': {
                  'cell_dynamics':'bfgs',
                  'cell_dofree':'x'
                  }})
        return parameters
    def get_pw_relax_parameters(self):
        
        ParameterData = DataFactory('parameter')
        parameters = ParameterData(dict={
                  'CONTROL': {
                      'calculation': 'relax',
                      'restart_mode': 'from_scratch',
                      'wf_collect': True,
                      'tstress':True,
                      'forc_conv_thr':0.0001,
                      'nstep':500,
                      },
                  'SYSTEM': {
                      'ecutwfc': 50.,
                      'ecutrho': 400.,
                      'occupations':'smearing',
                      'degauss': 0.001,
                      },
                  'ELECTRONS': {
                      'conv_thr': 1.e-8,
                      'mixing_beta':0.25,
                      'electron_maxstep':50,
                      'scf_must_converge':False,
                      },
                  'IONS':{}})
        return parameters
    
    
    def get_pw_energy_parameters(self):
        ParameterData = DataFactory('parameter')
        parameters = ParameterData(dict={
                  'CONTROL': {
                      'calculation': 'scf',
                      'restart_mode': 'from_scratch',
                      'wf_collect': True,
                      'forc_conv_thr':0.0001,
                      'nstep':500,
                      },
                  'SYSTEM': {
                      'ecutwfc': 50.,
                      'ecutrho': 400.,
                      'occupations':'smearing',
                      'degauss': 0.001,
                      'nspin':2,
                      },
                  'ELECTRONS': {
                      'conv_thr': 1.e-8,
                      'mixing_beta':0.25,
                      'electron_maxstep':500,
                      'scf_must_converge':False,
                      }})
        return parameters

    def get_bands_parameters(self):
        ParameterData = DataFactory('parameter')
        parameters = ParameterData(dict={
                  'CONTROL': {
                      'calculation': 'bands',
                      'tstress': True,
                      'wf_collect': True,
                      'forc_conv_thr':0.0001,
                      'nstep':500,
                      },
                  'SYSTEM': {
                      'ecutwfc': 50.,
                      'ecutrho': 400.,
                      'occupations':'smearing',
                      'degauss': 0.001,
                      'nspin':2,
                      },
                  'ELECTRONS': {
                      'conv_thr': 1.e-8,
                      'mixing_beta':0.25,
                      'electron_maxstep':100,
                      'scf_must_converge':False,
                  }})
        return parameters

        
    def get_pp_parameters(self):
        ParameterData = DataFactory('parameter')
        parameters = ParameterData(dict={
                  'inputpp':{
                  'outdir':'./out',
                  'filplot':'hartree.unformatted',
                  'plot_num':11,
                  'prefix':'aiida',
                  },
                  'plot':{
                  'nfile':1,
                  'iflag':3,
                  'output_format':6,
                  'fileout':'hartree.cube',
                  }}).store()
        return parameters
    def get_bands_postproc_parameters(self):
        ParameterData = DataFactory('parameter')
        parameters = ParameterData(dict={
                  'bands': {
                  'outdir':'./out',
                  'filband':'mol.band',
                  'lsym':True,
                  'prefix':'aiida',
                  }}).store()
        return parameters
                

    def get_kpoints(self,nx=1,ny=1,nz=1, set_list=False):
        if nx < 1.0:
            nx=1
        if ny < 1.0:
            ny=1
        if nz < 1.0:
            nz=1
        KpointsData = DataFactory('array.kpoints')
        kpoints = KpointsData()
        if set_list is False:
            kpoints.set_kpoints_mesh([nx,ny,nz], offset=[0.0,0.0,0.0] )
        else:
            points=[]
            for i in np.linspace(0,0.5,nx):
                points.append([i,0.0, 0.0])
            kpoints.set_kpoints(points)
        return kpoints




    def get_pw_calculation(self, pw_structure, rem_folder,
                           pw_parameters,pw_kpoints,
                           num_machines=1):
        ParameterData = DataFactory('parameter')
        params = self.get_parameters()


        pw_codename = params['pw_codename']
        max_wallclock_seconds  = params['max_wallclock_seconds']
        num_mpiprocs_per_machine   = params['num_mpiprocs_per_machine']
        pseudo_family = params['pseudo_family']


        code = Code.get_from_string(pw_codename)
        calc = code.new_calc()
        calc.set_max_wallclock_seconds(max_wallclock_seconds)
        calc.set_resources({"num_machines":num_machines,"num_mpiprocs_per_machine": num_mpiprocs_per_machine})
        if 'queue' in params:
            calc.set_queue_name(params['queue'])
        if rem_folder :
            calc.use_parent_folder(rem_folder)
        
        npools=getpools(nodes=num_machines,cpu_node=num_mpiprocs_per_machine,kpoints=pw_kpoints)

        settings_dict={'cmdline':["-npools",str(npools)]}
        
        if pw_parameters.dict.CONTROL['calculation']=='bands':
            settings_dict['also_bands']=True
            settings_dict['cmdline']=["-npools","2"]
        if  pw_parameters.dict.CONTROL['calculation']=='scf':
            start_mag={}
            for i in  pw_structure.kinds:
                if i.name.endswith("1"):
                    start_mag[i.name]=1.0
                elif i.name.endswith("2"):
                    start_mag[i.name]=-1.0
                else:
                    start_mag[i.name]=0.0
            pw_parameters.dict['SYSTEM']['starting_magnetization']=start_mag

        pw_parameters.store()
        settings = ParameterData(dict=settings_dict)
        calc.use_settings(settings)
        calc.use_code(code)
        calc.use_structure(pw_structure)
        calc.use_pseudos_from_family(pseudo_family)
        calc.use_parameters(pw_parameters)
        calc.use_kpoints(pw_kpoints)
        
        calc.store_all()
        return calc
    def get_pp_calculation(self, rem_folder, pp_parameters):
        params = self.get_parameters()
        pp_codename = params['pp_codename']
        max_wallclock_seconds  = params['max_wallclock_seconds']
        num_mpiprocs_per_machine   = params['num_mpiprocs_per_machine']


        code = Code.get_from_string(pp_codename)
        calc = code.new_calc()
        calc.set_max_wallclock_seconds(max_wallclock_seconds)
        calc.set_resources({"num_machines":1,"num_mpiprocs_per_machine": num_mpiprocs_per_machine})
        calc.use_parent_folder(rem_folder)
        if 'queue' in params:
            calc.set_queue_name(params['queue'])
        calc.store()

        calc.use_code(code)
        calc.use_parameters(pp_parameters)
        return calc

    def get_bands_postproc_calculation(self, rem_folder, bands_parameters, num_machines):
        params = self.get_parameters()
        bands_codename = params['bands_codename']
        max_wallclock_seconds  = params['max_wallclock_seconds']
        num_mpiprocs_per_machine   = params['num_mpiprocs_per_machine']


        code = Code.get_from_string(bands_codename)
        calc = code.new_calc()
        calc.set_max_wallclock_seconds(max_wallclock_seconds)
        calc.set_resources({"num_machines":num_machines,"num_mpiprocs_per_machine": num_mpiprocs_per_machine})
        calc.use_parent_folder(rem_folder)
        if 'queue' in params:
            calc.set_queue_name(params['queue'])
        calc.store()

        calc.use_code(code)
        calc.use_parameters(bands_parameters)
        return calc
        

    @Workflow.step
    def cell_opt(self):
       # from aiida.orm import Code, Computer, CalculationFactory
        #aiidalogger.info("Running a unit cell optimization job")
        self.append_to_report("Running a unit cell optimization job")
        params = self.get_parameters()
        struct=self.get_structure()
        x_cell=struct.cell[0][0]
        calc =        self.get_pw_calculation(struct,
                                         rem_folder=None,
                                         pw_parameters=self.get_pw_vc_parameters(),
                                         pw_kpoints=self.get_kpoints(round(30/x_cell*1)), num_machines=params['nodes'])


        self.attach_calculation(calc)
        self.next(self.cell_opt2)
    @Workflow.step
    def cell_opt2(self):
       # from aiida.orm import Code, Computer, CalculationFactory
        #aiidalogger.info("Running a unit cell optimization job")
        self.append_to_report("Running a unit cell optimization job")
        params = self.get_parameters()
        start_calc=self.get_step_calculations(self.cell_opt)[0]
        struct=start_calc.out.output_structure
        x_cell=struct.cell[0][0]
        calc =        self.get_pw_calculation(struct,
                                         rem_folder=None,
                                         pw_parameters=self.get_pw_vc_parameters(),
                                         pw_kpoints=self.get_kpoints(round(30/x_cell*1)), num_machines=params['nodes'])
        self.attach_calculation(calc)
        self.next(self.relax)
    @Workflow.step
    def relax(self):
        self.append_to_report("Reoptimizing the geometry with more k-points")
        params = self.get_parameters()
        start_calc=self.get_step_calculations(self.cell_opt2)[0]
        struct=start_calc.out.output_structure
        x_cell=struct.cell[0][0]
        calc =        self.get_pw_calculation(struct,
                                         rem_folder=None,
                                         pw_parameters=self.get_pw_relax_parameters(),
                                         pw_kpoints=self.get_kpoints(round(30/x_cell*6)), num_machines=params['nodes'])

        self.attach_calculation(calc)
        self.next(self.scf)
    @Workflow.step
    def scf(self):
        self.append_to_report("Recomputing charge-density with more k-points in the optimized geometry")
        params = self.get_parameters()
#Uncomment this for a production run:
        start_calc=self.get_step_calculations(self.relax)[0]
        struct=start_calc.out.output_structure
#Uncomment this for a test run:
#        s=self.get_structure()
        x_cell=struct.cell[0][0]
        calc =        self.get_pw_calculation(struct,
                                         rem_folder=None,
                                         pw_parameters=self.get_pw_energy_parameters(),
                                         pw_kpoints=self.get_kpoints(round(30/x_cell*12)), num_machines=params['nodes'])


        self.attach_calculation(calc)
        self.next(self.plot_hartree)

    @Workflow.step
    def plot_hartree(self):
        self.append_to_report("Plotting hartree potential")
        start_calc=self.get_step_calculations(self.scf)[0]
        calc= self.get_pp_calculation(rem_folder=start_calc.out.remote_folder,
                                      pp_parameters=self.get_pp_parameters() )
        self.attach_calculation(calc)
        self.next(self.bands)
    @Workflow.step
    def bands(self):
        self.append_to_report("Band structure calculations")
        params = self.get_parameters()
#Uncomment this for a production run:
        start_calc=self.get_step_calculations(self.scf)[0]
#Uncomment this for a test run:
#        start_calc=load_node(2348)


        struct=start_calc.inp.structure
        x_cell=struct.cell[0][0]
        calc =        self.get_pw_calculation(struct,
                                         rem_folder=start_calc.out.remote_folder,
                                         pw_parameters=self.get_bands_parameters(),
                                         pw_kpoints=self.get_kpoints(nx=round(30/x_cell*30),
                                         set_list=True), num_machines=params['nodes'])

        self.attach_calculation(calc)
        self.next(self.exit)


