#!/usr/bin/python
from aiida.workflows.scan_nanoribbon  import ScananoribbonWorkflow
import os.path
import sys


cn='./calc_number.txt'
if len(sys.argv) == 2:
    n=sys.argv[1]
else:
    if os.path.isfile(cn) :
        with open(cn) as num:
            n=int(num.readline().split()[0])+1
        with open(cn,'w') as num:
            num.write('{}'.format(n))
    else :
        n=1
        with open(cn,'w') as num:
            num.write('{}'.format(n))

coordfname='structures/'+str(n)+'.xyz'



params = {'pw_codename':'pw5.2@dora','pp_codename':'pp5.2@dora',
          'bands_codename':'bands5.2@hypatia','num_mpiprocs_per_machine':36,
          'nodes':4,
          'max_wallclock_seconds':24*60*60,'pseudo_family':'doping_atoms',
#          'queue':'newshort','inpcoord':coordfname}
          'inpcoord':coordfname}
#print params
wf=ScananoribbonWorkflow(params=params)
wf.store()
wf.cell_opt()
#wf.bands()


