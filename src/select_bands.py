from plot_b import plot_b,find_bandgap,get_calc_obj
import matplotlib.pyplot as plt

from aiida.orm.data.array.bands import BandsData
from aiida.orm.querybuilder import QueryBuilder
from optparse import OptionParser
import matplotlib.image as mpimg


parser = OptionParser()
parser.add_option( "--gap", action="store", type="float", help="Band gap range", nargs=2)
parser.add_option( "--vband", action="store", type="float", help="Valence band max energy range", nargs=2)
parser.add_option( "--cband", action="store", type="float", help="Conduction band min energy range", nargs=2)
parser.add_option( "--efermi", action="store", type="float", help="Fermi energy range", nargs=2)
parser.add_option( "--etotal", action="store", type="float", help="Total energy range", nargs=2)
parser.add_option( "--t_magn", action="store", type="float", help="Total magnetization range", nargs=2)
parser.add_option( "--a_magn", action="store", type="float", help="Absolute magnetization range", nargs=2)


(options, args) = parser.parse_args()
opt_dict = vars(options)




qb = QueryBuilder()
qb.append(BandsData)
res=[]
names=[]
str_list=[]
hartree_run=[]
bands_run = []
scf_run = []
band_gap_list = []


for node  in qb.iterall():
    print node
    bands_calc_obj,scf_calc_obj, hartree_calc_obj,struct=get_calc_obj(node[0])
    try:
        a_magn=scf_calc_obj.res['absolute_magnetization']
        t_magn=scf_calc_obj.res['total_magnetization']
        fermi_energy= bands_calc_obj.res['fermi_energy']
        vac_lev=0.0
        if hartree_calc_obj != None:
            vac_lev=hartree_calc_obj.res['vacuum_level']*27.211385/2.0
        out=find_bandgap(node[0], fermi_energy=fermi_energy)
        fermi_energy-=vac_lev
        gap=out[1]
    except:
#        print "bla"
        continue

    if out[0] != False:
        homo=out[2]-vac_lev
        lumo=out[3]-vac_lev
    if opt_dict['gap'] != None:
        if gap < opt_dict['gap'][0] or gap > opt_dict['gap'][1]:
            continue
    if opt_dict['vband'] != None:
        if homo < opt_dict['vband'][0] or homo > opt_dict['vband'][1]:
            continue
    if opt_dict['cband'] != None:
        if lumo < opt_dict['cband'][0] or lumo > opt_dict['cband'][1]:
            continue
    if opt_dict['efermi'] != None:
        if fermi_energy < opt_dict['efermi'][0] or fermi_energy > opt_dict['efermi'][1]:
            continue
    if opt_dict['t_magn'] != None:
        if t_magn < opt_dict['t_magn'][0] or t_magn > opt_dict['t_magn'][1]:
            continue
    if opt_dict['a_magn'] != None:
        if a_magn < opt_dict['a_magn'][0] or a_magn > opt_dict['a_magn'][1]:
            continue
    res.append(node[0])
    names.append(''.join(['%s%s' % (key, value) for (key, value) in  struct.get_composition().items()]))
    str_list.append(struct)
    hartree_run.append(hartree_calc_obj)
    bands_run.append(bands_calc_obj)
    scf_run.append(scf_calc_obj)
    band_gap_list.append(gap)


from matplotlib.gridspec import GridSpec

fig=plt.figure()
fig.set_size_inches(2.8*len(res), 16)

gs=GridSpec(5, len(res))

fig.text(0.1, 0.6, 'E(eV)', va='center', rotation='vertical')


i=0
for node in res:
    if i== 0:
        ax1=fig.add_subplot(gs[1:5,i])
    else:
        ax1=fig.add_subplot(gs[1:5,i], sharey=ax_old)
        ax1.tick_params(axis='y', which='both',left='on',right='off', labelleft='off')
        
    plot_b(hartree_run[i], bands_run[i],scf_run[i],ax1)
#    ax1.xaxis('off')
#    ax1.tick_params(axis='x', which='both', bottom='off', top='off',  labelbottom='off')
    ax1.tick_params(axis='x', which='both', bottom='off', top='off' )
    labels = [item.get_text() for item in ax1.get_xticklabels()]
    labels[0]='$\Gamma$'
    labels[-1]='X'
    ax1.set_xticklabels(labels)
    
    try:
        magn=scf_run[i].res['absolute_magnetization']
        magn_units=scf_run[i].res['absolute_magnetization_units']
        tot_magn = scf_run[i].res['total_magnetization']
        tot_magn_units = scf_run[i].res['total_magnetization_units']
        gap = band_gap_list[i]
        if gap == None:
            gap=0.0
        ax1.set_xlabel('-Abs. magn.: {}$\mu_B$\n-Tot. magn.: {}$\mu_B$\n-Band gap: {:.3f} eV'.format(magn,tot_magn,gap))
    except :
        print ("Oops!")
        print scf_run[i].id
     
    s=str_list[i].get_ase()
    s.write("molecule.png")

    ax2=fig.add_subplot(gs[0:1,i])
    image=mpimg.imread('./molecule.png'.format(i))
    ax2.imshow(image)
    ax2.axes.get_yaxis().set_visible(False)
    ax2.set_xlabel('{}'.format(names[i]), fontsize=12)
    ax2.tick_params(axis='x', which='both', bottom='off', top='off',labelbottom='off')
    ax_old=ax1

    i+=1
plt.savefig('bands.png')
plt.show()

